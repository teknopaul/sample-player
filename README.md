Sample Player
-------------

Single page HTML5 app that allows hitting the numeric keys on a computer keyboard to trigger samples.

Essentially a cross platform sampler that relies on your browsers ability to play audio.

Written because professional samplers in the world of Linux require Jack and complicated setup I couldn't be arsed to go thorugh.

Caveats
=======

Plays only what your browser plays, 16bit .wav files are corss platform, mp3s are not.

Latency is probably not good enough for playing music via this app.

Configured via a JSON file.

Copyright
=========

Copyright teknopaul 2014 GPLv3

